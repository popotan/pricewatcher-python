from flask import Flask, send_from_directory, session, request, render_template
from flask_bcrypt import Bcrypt
from priceWatcher.controller.main import main
from priceWatcher.controller.user import user
from priceWatcher.controller.sheet import sheet
from priceWatcher.controller.config import config

from priceWatcher.database import db_session

app = Flask(__name__, static_url_path='')
app.secret_key = 'priceWatcher-python'

bcrypt = Bcrypt(app)

@app.errorhandler(500)
def page_not_found(error):
	return render_template('err/500.html'), 500

@app.teardown_request
def shutdown_session(exception=None):
    db_session.remove()

@app.route('/static/<path:path>')
def send_js(path):
    return send_from_directory('static/', path)

app.register_blueprint(main, url_prefix='')
app.register_blueprint(user, url_prefix='/user')
app.register_blueprint(sheet, url_prefix='/sheet')
app.register_blueprint(config, url_prefix='/config')
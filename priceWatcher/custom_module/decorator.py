from functools import wraps
from flask import session

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'valid' in session.keys():
         if session['valid']:
         	return f(*args, **kwargs)
        return ('', 401)
       
    return decorated_function
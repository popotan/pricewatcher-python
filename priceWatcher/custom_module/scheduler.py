class Config(object):
	JOBS = [{
	'id': 'send_sms',
	'func': 'priceWatcher.scheduler_jobs:send_sms',
	'args': (),
	'trigger': 'interval',
	# 'seconds': 3600
	'seconds': 30
	}]

	SCHEDULER_VIEWS_ENABLED = True

from datetime import datetime, timedelta, date
class SCmodule(object):
	def get_minute_margin():
		now = datetime.utcnow() + timedelta(hours=9)
		target_hour = now.replace(hour=now.hour+1, second=0)
		marginal_minute_for_next_hour = target_hour - now
		print(marginal_minute_for_next_hour.minutes)
		return marginal_minute_for_next_hour.minutes
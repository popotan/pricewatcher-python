from flask import Blueprint, render_template, session, jsonify, request
from priceWatcher.database import init_db
from priceWatcher.database import db_session

import json
from httplib2 import Http
import datetime
import urllib.parse
import urllib.request
import xmltodict

class NaverAPI(object):
	uri = ''
	mid = ''
	well_formed_result = {
		'lprice' : 0,
		'hprice': 0,
		'image' : ''
	}

	def set_mid(self, mid):
		self.mid = mid
		return self

	def set_url(self):
		url = 'https://openapi.naver.com:443/v1/search/shop.xml?query=' + self.mid
		url += '&display=1&start=1&sort=sim'
		self.uri = url
		return self

	def parse_mid(self, url):
		result = ''
		querysStr = url.split("?")[1]
		querysArr = querysStr.split("&")
		for qs in querysArr:
			queryKeyVal = qs.split("=")
			if queryKeyVal[0] == 'nv_mid':
				result = queryKeyVal[1]
				break
		return result

	def request(self):
		req = urllib.request.Request(self.uri, headers={'X-Naver-Client-Id':'L8kbV4VZl7x6Cw_qvFBh', 'X-Naver-Client-Secret': 'MeJTMX_QQ6'})
		xml_result = urllib.request.urlopen(req).read().decode("utf8")
		tree = xmltodict.parse(xml_result)
		print(tree)
		if len(tree['rss']['channel']['item']) > 0:
			self.dict_result = tree['rss']['channel']['item']
		else:
			self.dict_result = []
		return self

	def well_formed(self):
		self.well_formed_result['lprice'] = self.dict_result['lprice']
		self.well_formed_result['hprice'] = self.dict_result['hprice']
		self.well_formed_result['image'] = self.dict_result['image']
		return self
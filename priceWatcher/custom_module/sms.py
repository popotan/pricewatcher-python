import datetime, json
import urllib
import xmltodict

class Sms():
	params = {
	'id': 'funclass',
	'pwd': 'O99RWO101070EWAT4450',
	'from': '',
	'to_country': '82',
	'to': '',
	'message': '',
	'report_req': '1'
	}

	def __init__(self, title=None, to_number=None, from_number='', desc=''):
		self.params['title'] = title
		self.params['to'] = to_number
		self.params['from'] = from_number
		self.params['message'] = desc

	def send(self):
		url = 'http://rest.supersms.co:6200/sms/xml?' + urllib.parse.urlencode(self.params)
		
		response = urllib.request.urlopen(url).read().decode('utf8')
		tree = xmltodict.parse(response)
		print(tree)
		if tree['submit_response']['messages']['message']['err_code'] == 'R000':
			self.result = True
		else:
			self.result = False
		return self
from flask import Blueprint, render_template, session, jsonify, request
from priceWatcher.database import init_db
from priceWatcher.database import db_session

import gspread, os
from oauth2client.service_account import ServiceAccountCredentials
import json
from httplib2 import Http
import datetime
import urllib.parse
import urllib.request
import xmltodict

class GoogleAPI(object):
	"""docstring for GoogleAPI"""
	def __init__(self, sheetid):
		# 다음을 사용하기 전에 발급받은 json키의 client_email로 Spreadsheet를 공유해야 한다.
		SCOPE = ["https://spreadsheets.google.com/feeds"]
		# json키 저장해서 써야함.
		SECRETS_FILE = os.path.realpath(os.path.dirname(__file__)) + "/../googleAPIkey.json"
		# Spreadsheet 이름
		SPREADSHEET = sheetid
		credentials = ServiceAccountCredentials.from_json_keyfile_name(SECRETS_FILE, SCOPE)

		gc = gspread.authorize(credentials)
		worksheet = gc.open_by_key(SPREADSHEET).sheet1
		# 모든 데이터 긁어오기
		self.rows = worksheet.get_all_values()
		self.split_data()

	def split_data(self):
		self.dict_result = []
		for data in self.rows[6:]:
			self.dict_result.append(data)
		return self
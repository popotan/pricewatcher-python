from flask import Blueprint, render_template, session, jsonify, request
from priceWatcher.database import init_db
from priceWatcher.database import db_session

import json
from httplib2 import Http
import datetime
import urllib.parse
import urllib.request
from io import StringIO

class DaumAPI(object):
	uri = ''
	mid = ''
	well_formed_result = {
		'lprice' : 0,
		'hprice': 0,
		'image' : ''
	}

	def set_mid(self, mid):
		self.mid = mid
		return self

	def set_url(self):
		self.uri = 'https://apis.daum.net/shopping/detail?apikey=467677335da0d33c483c2c5082b7e9d1&output=json&docid=' + self.mid
		return self

	def parse_mid(self, url):
		segment = url.split("//")[1].split('/')
		if segment[1] == 'product':
			return segment[2]
		else:
			return ''

	def request(self):
		req = urllib.request.Request(self.uri)
		result = urllib.request.urlopen(req).read().decode("utf8")
		io = StringIO(result)
		result = json.load(io)
		if len(result['channel']['item']):
			self.dict_result = result['channel']['item'][0]
		else:
			self.dict_result = {}
		return self

	def well_formed(self):
		self.well_formed_result['lprice'] = self.dict_result['price_min']
		self.well_formed_result['hprice'] = self.dict_result['price_max']
		self.well_formed_result['image'] = self.dict_result['image_url']
		return self
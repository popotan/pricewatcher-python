class DomainParser(object):
	uri = ''
	result = ''

	def __init__(self, url):
		self.uri = url

	def parse(self):
		self.uri = self.uri.split('//')[1]
		domain = self.uri.split('/')[0].split('.')
		print(domain)
		if 'naver' in domain:
			self.result = 'naver'
		elif 'daum' in domain:
			self.result = 'daum'
		else:
			self.result = ''
		return self
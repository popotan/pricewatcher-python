angular.module('soundPriceMonitor',
	['ngRoute'])
.config(function($httpProvider){
	$httpProvider.useApplyAsync(true);
})
.run(function($rootScope, $http, $location, $compile){
	$rootScope.showModal = function(domElementTagName, scope){
		angular.element(".modal").append($compile(domElementTagName)(scope)).fadeIn("fast");
	}
	$rootScope.closeModal = function(){
		angular.element(".modal").fadeOut().empty();
	}
	$rootScope.session = false;
	$rootScope.getSession = function(){
		$http.get('/user')
		.then(function(res){
			switch(res.status){
				case 200:
				$rootScope.session = true;
				return true;
				break;
				case 401:
				$rootScope.session = false;
				$location.url('/login');
				return false;
				break;
				default:
				return true;
				break;
			}
		});
	}

	// $rootScope.$on('$locationChangeStart', function(next, current) {
	// 	$rootScope.getSession();
	// });
});
angular.module("soundPriceMonitor")

.controller("headerController", function($http){
	var headerCtrl = this;

	headerCtrl.title = "사픽프랜즈 가격 모니터링 시스템";

	headerCtrl.sheetid = function(){
		$http.get('/sheet/uid')
		.success(function(sheetid){
			var url = 'https://docs.google.com/spreadsheets/d/'+sheetid+'/edit?usp=sharing';
			window.open(url, "_blank");
		});
	}

	headerCtrl.sync = function(){
		alert("동기화 기능은 추후 제공됩니다. 현재는 엑셀시트에 입력된 자료가 바로 반영됩니다!");
	}
});
angular.module("soundPriceMonitor")

.controller('logoutController', function($rootScope, $scope, $http, $location){
	var logoutCtrl = this;
	
	$scope.post = function(){
		$http.post('/user/logout')
		.success(function(result){
			$location.path('/login');
		})
		.error(function(err){
			console.log(err);
		});
	};

	$scope.post();
});
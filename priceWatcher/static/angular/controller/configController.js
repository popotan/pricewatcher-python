angular.module('soundPriceMonitor')
.controller('configController', function($rootScope, $scope, $http){
	var configCtrl = this;

	$scope.sms_target_list = [];
	$scope.time_range = [];
	for (var i = 6; i < 23; i++) {
		$scope.time_range.push(i);
	}
	$scope.selected_time = [];

	$scope.get_sms_target_list = function(){
		$http.get('/config/sms')
		.success(function(result){
			$scope.sms_target_list = result;
		});
	};

	$scope.add_sms_target_number_row = function(){
		$scope.sms_target_list.push({
			"nickname": "",
			"send_switch": 1,
			"target_phone_number": ""
		});
	};
	$scope.get_sms_target_list();

	$scope.$on('row:add', function(event, data){
		for (var i = $scope.sms_target_list.length - 1; i >= 0; i--) {
			if($scope.sms_target_list[i].$$hashKey == data.hashKey){
				$scope.sms_target_list[i] = data.result;
			}
		}
	});

	$scope.click_schedule = function(time){
		var is_selected = $scope.check_is_time_selected(time);
		if (is_selected) {
			time_schedule_delete(time);
		}else{
			time_schedule_append(time);
		}
	}

	time_schedule = function(time){
		$http.get('/config/target/time')
		.success(function(result){
			$scope.selected_time = result;
		});
	};
	time_schedule();

	$scope.check_is_time_selected = function(time){
		var is_selected = false;
		for (var i = 0; i < $scope.selected_time.length; i++) {
			if($scope.selected_time[i] == time){
				is_selected = true;
				break;
			}
		}
		return is_selected;
	}

	time_schedule_append = function(time){
		console.log('append');
		$http.post('/config/target/time', {target_time : time})
		.success(function(result){
			time_schedule();
		});
	};

	time_schedule_delete = function(time){
		console.log("delete");
		$http.delete('/config/target/time', {params : {target_time : time}})
		.success(function(result){
			time_schedule();
		});
	};
});
angular.module("soundPriceMonitor")

.controller("sideController", function($rootScope, $http, $scope){
	console.log('sideController loaded');
	var sideCtrl = this;

	$scope.notice_list = [];

	$scope.get_notice = function(){
		$http.get('/api/get_notice')
		.success(function(result){
			console.log(result);
			$scope.notice_list = result;
		})
		.error(function(err){
			console.log(err);
		});
	};

	$scope.goto_alert = function(productNo){
		var position = angular.element('product[no=' + productNo + ']').offset().top;
		var ngViewPosition = angular.element('ng-view').scrollTop();
		position = parseInt(position);

		//스크롤이동
		angular.element('ng-view').animate({scrollTop : ngViewPosition + position - 150}, 500);
		//깜박임
		angular.element('product[no=' + productNo + ']').animate({borderLeftWidth : 0}, 300).animate({borderLeftWidth : '0.5rem'}, 300);
	};
});
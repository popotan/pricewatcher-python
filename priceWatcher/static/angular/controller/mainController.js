angular.module('soundPriceMonitor')

.controller('mainController', function($rootScope, $scope, $http, $compile, $location, $interval){
	var mainCtrl = this;

	$scope.brand = [];
	$scope.item = [];

	$rootScope.alertedProduct = [];
	$rootScope.parse_cnt = 0;
	$scope.parse_interval = 60;

	var doParse = $interval(function(){mainCtrl.parseCntAdd();}, 1000 * 60 * $scope.parse_interval);

	mainCtrl.parseCntAdd = function(){
		$rootScope.parse_cnt++;
	}

	$scope.$watch('parse_interval', function(newval, oldval){
		if (newval==oldval) {return;}
		$interval.cancel(doParse);
		$interval(function(){mainCtrl.parseCntAdd();}, 1000 * 60 * $scope.parse_interval);
	});

	$scope.brandMerge = function(){
		var mergedBrand = [];
		for (var i = 0; i < $scope.brand.length; i++) {
			if(mergedBrand.indexOf($scope.brand[i]) == -1){
				mergedBrand.push($scope.brand[i]);
			}
		}
		$scope.brand = mergedBrand;
		$scope.makeProductsDirective();
	}

	$scope.makeProductsDirective = function(){
		for (var i = 0; i < $scope.brand.length; i++) {
			angular.element("#main #products-wrapper")
			.append($compile('<products brand=' + $scope.brand[i].replace(" ", "_") + '></products>')($scope));
		}
	}

	$scope.load_sheet = function(){
		$http.get('/sheet')
		.then(function(res){
			switch(res.status){
				case 200:
				var result = res.data;
				$scope.item = result;
				if ($scope.item.length == 0) {
					alert("자료가 없습니다. 엑셀시트에 자료를 입력해주세요!");
				}
				for (var i = 0; i < result.length; i++) {
					$scope.brand.push(result[i][0]);
				}
				$scope.brandMerge();
				angular.element('#loading').hide();
				return true;
				break;

				case 401:
				alert("로그인이 필요합니다.");
				return false;
				break;

				case 500:
				alert("서버에 오류가 발생했습니다.");
				return false;
				break;
			}
		});
	}
	
	$scope.firstLoad = function(){
		console.log('firstload');
		$scope.load_sheet();
		angular.element('#brand-clone-header').width(angular.element('#main').innerWidth());
	}
	$scope.firstLoad();

	angular.element('#main').scroll(function(){
		var main = angular.element(this);
		var header = angular.element('#brand-clone-header');
		if(main.scrollTop() <= 8){
			header.fadeOut('fast');
		}else{
			header.fadeIn('fast');
		}
	});

	angular.element(window).resize(function(){
		angular.element('#brand-clone-header').width(angular.element('#main').innerWidth());
	});
});
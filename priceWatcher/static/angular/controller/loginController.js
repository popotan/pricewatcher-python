angular.module("soundPriceMonitor")

.controller("loginController", function($rootScope, $http, $location, $scope){
	var loginCtrl = this;

	$scope.id = '';
	$scope.password = '';

	$scope.post = function(){
		$http.post( '/user/login', 
			JSON.stringify({
				id : $scope.id,
				password : $scope.password
			}),
			{ headers: {'Content-Type': 'application/json'} })
		.success(function(result){
				$rootScope.closeModal();
				$location.path("/");
		})
		.error(function(err){
			alert("접속오류!");
		});
	};
});
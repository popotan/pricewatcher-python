angular.module("soundPriceMonitor")

.directive("product", function($rootScope, $http, $interval, stringParser){
	return {
		restirct : "E",
		templateUrl : "angular/template/product.html",
		scope : {},
		// replace : true,
		link: function(scope, element, attrs) {
			
		},
		controller : function($scope, $element, $attrs){
			$scope.product = {
				title : $scope.$parent.item[$attrs.no][1],
				no : $attrs.no,
				optLowPrice : parseInt($scope.$parent.item[$attrs.no][2].replace(/,/g,"")),
				optHighPrice : $scope.$parent.item[$attrs.no][3],
				url : $scope.$parent.item[$attrs.no][4],
				lprice : 0,
				hprice : 0,
				marginal : 0,
				imageUrl : ''
			};

			$scope.priceAlert = function(marginal){
				if(marginal < 0){
					$element.addClass("alert");
					return true;
				}else{
					$element.removeClass("alert");
					return false;
				}
			}

			$scope.pushAlertedProductArr = function(){
				var datetime = new Date();
				var day = addZero(datetime.getDate());
				var time = addZero(datetime.getHours());
				var minute = addZero(datetime.getMinutes());
				$scope.product.datetime = day + '일 ' + time + ':' + minute;
				$rootScope.alertedProduct.push($scope.product);
			}

			var addZero = function(i) {
				if (i < 10) {
					i = "0" + i;
				}
				return i;
			}

			$scope.naverAPI = function(){
				$http.get('/sheet/parse/price', { 
					params : { url : encodeURIComponent($scope.$parent.item[$attrs.no][4]) } 
				})
				.success(function(result){
					var excelLowPrice =  $scope.$parent.item[$attrs.no][2];
					if (typeof excelLowPrice == 'string') {
						excelLowPrice = parseInt($scope.$parent.item[$attrs.no][2].replace(/,/g,""));
					}
					$scope.product.lprice = result.lprice;
					$scope.product.hprice = result.hprice;
					$scope.product.marginal = parseInt(result.lprice) - excelLowPrice;
					$scope.product.imageUrl = result.image;

					if($scope.priceAlert($scope.product.marginal)){
						$scope.pushAlertedProductArr();
					}
				})
				.error(function(err){
					console.log("정보가져오기에 실패하였습니다.");
				});
			}
			$scope.naverAPI();

			$rootScope.$watch('parse_cnt', function(newval, oldval){
				if (newval==oldval) {return;}
				$scope.naverAPI();
			});
		}
	};
})

.service("stringParser", function(){
	var service = this;

	service.mid = function(url){
		var querysStr = url.split("?")[1];
		var querysArr = querysStr.split("&");
		for (var k = 0; k < querysArr.length; k++) {
			var queryKeyVal = querysArr[k].split("=");
			if(queryKeyVal[0] == 'nv_mid'){
				return queryKeyVal[1];
			}
		}
	}
});
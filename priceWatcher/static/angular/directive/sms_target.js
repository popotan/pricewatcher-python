angular.module("soundPriceMonitor")

.directive("smsTarget", function($rootScope, $http, $interval, stringParser){
	return {
		restirct : "A",
		templateUrl : "angular/template/sms-target.html",
		scope : {list : '='},
		// replace : true,
		link: function(scope, element, attrs) {
			if (typeof scope.list.id !== 'undefined') {
				var inputbox_name = element.children('td').children('input[type=text]');
				var inputbox_tel = element.children('td').children('input[type=tel]');
				inputbox_name.attr('disabled', 'disabled');
				inputbox_tel.attr('disabled', 'disabled');
			}
		},
		controller : function($scope, $element, $attrs){
			$scope.add_target = function(){
				$http.post('/config/target/sms', JSON.stringify($scope.list))
				.success(function(result){
					$scope.$emit('row:add', { result : result, hashKey : $scope.list.$$hashKey });
				});
			}

			$scope.remove_target = function(){
				if (typeof $scope.list.id !== 'undefined') {
					if (confirm('해당 연락처를 sms 수신 대상에서 제외하시겠습니까?')) {
						$http.delete('/config/target/sms', 
							{ params : {id : $scope.list.id} })
						.success(function(result){
							$scope.$parent.sms_target_list.splice($scope.$parent.$index, 1);
							$element.remove();
						});
					}
				}else{
					$scope.$parent.sms_target_list.splice($scope.$parent.$index, 1);
					$element.remove();
				}
			};

			$scope.switch_send_status = function(){
				if($scope.list.send_switch == 1){
					$scope.list.send_switch = 0;
				}else{
					$scope.list.send_switch = 1;
				}
				$scope.send_switch();
			}

			$scope.send_switch = function(){
				$http.put('/config/target/sms', JSON.stringify($scope.list))
				.success(function(result){
				});
			}
		}
	};
});
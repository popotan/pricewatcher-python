angular.module("soundPriceMonitor")

.directive("products", function($rootScope, $compile){
	return {
		restirct : "E",
		templateUrl : "angular/template/products.html",
		scope : true,
		// replace : true,
		link: function(scope, element, attrs) {
			scope.brand_title = attrs.brand.replace("_", " ");
			for (var i = 0; i < scope.item.length; i++) {
				if(attrs.brand.replace("_", " ") == scope.item[i][0]){
					element.append($compile('<product no='+i+'></product>')(scope));
				}
			}
		},
		controller : function($scope, $element, $attrs){
			angular.element('#main').scroll(function(){
				if($element.position().top <= 0){
					angular.element('#brand-clone-header').empty();
					$element.children('.brand-header')
					.clone().appendTo('#brand-clone-header');
				}
			});
		}
	};
});
angular.module("soundPriceMonitor")

.config(function($routeProvider, $provide, $httpProvider){
	$routeProvider
	.when('/', {
		templateUrl : "/static/angular/template/main.html",
		controller : "mainController"
	})
	.when('/login', {
		templateUrl : "/static/angular/template/login.html",
		controller : "loginController"
	})
	.when('/logout', {
		template : "<div>로그아웃중</div>",
		controller : "logoutController"
	})
	.when('/config', {
		templateUrl : '/static/angular/template/config.html',
		controller : 'configController'
	});
	$httpProvider.interceptors.push('statusInterceptor');
})
.factory('statusInterceptor', function($location){
	return {
		request: function(config) {
			return config;
		},
		requestError: function(config) {
			return config;
		},
		response: function(res) {
			return res;
		},
		responseError: function(res) {
			switch(res.status){
				case 401:
				alert("세션이 만료되었습니다. 로그인페이지로 이동합니다.");
				$location.url('/login');
				return res;
				break;
				default:
				return res;
				break;
			}
		}
	};
});
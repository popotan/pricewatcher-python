from flask import Blueprint, render_template, session, jsonify, request
from priceWatcher.database import init_db
from priceWatcher.database import db_session

from priceWatcher.custom_module.decorator import login_required
from priceWatcher.custom_module.naverAPI import NaverAPI
from priceWatcher.custom_module.daumAPI import DaumAPI
from priceWatcher.custom_module.googleAPI import GoogleAPI
from priceWatcher.custom_module.domainParser import DomainParser
from urllib import parse

sheet = Blueprint('sheet', __name__)

@sheet.route('/')
@login_required
def index():
	if 'sheetid' in session:
		sheet = GoogleAPI(sheetid=session['sheetid'].decode('utf8'))
		return jsonify(sheet.dict_result)
	else:
		return ('', 401)

@sheet.route('/parse/price', methods=['GET'])
@login_required
def parse_price():
	url = parse.unquote(request.args.get('url'))
	domain = DomainParser(url=url).parse().result
	if domain == 'naver':
		return get_from_naver_API()
	elif domain == 'daum':
		return get_from_daum_API()
	else:
		return ('', 204)

def get_from_naver_API():
	url = parse.unquote(request.args.get('url'))
	naver_data = NaverAPI()
	mid = naver_data.parse_mid(url=url)
	naver_data.set_mid(mid=mid).set_url()
	return jsonify(naver_data.request().well_formed().well_formed_result)

def get_from_daum_API():
	url = parse.unquote(request.args.get('url'))
	daum_data = DaumAPI()
	mid = daum_data.parse_mid(url=url)
	daum_data.set_mid(mid=mid).set_url()
	return jsonify(daum_data.request().well_formed().well_formed_result)

@sheet.route('/uid', methods=['GET'])
@login_required
def get_sheet_uid():
	if 'sheetid' in session:
		return jsonify(session['sheetid'].decode('utf8'))
	else:
		return jsonify('')
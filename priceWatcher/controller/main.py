from flask import Blueprint, render_template
from priceWatcher.database import init_db
from priceWatcher.database import db_session

main = Blueprint('main', __name__)

@main.route('/')
def index():
	return render_template("index.html")
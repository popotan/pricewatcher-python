from flask import Blueprint, render_template, session, jsonify, request
from priceWatcher.database import init_db
from priceWatcher.database import db_session

from priceWatcher.custom_module.sms import Sms
from priceWatcher.custom_module.decorator import login_required

config = Blueprint('config', __name__)

@config.route('/')
def index():
	return 200

@config.route('/sms', methods=['GET'])
@login_required
def sms_list():
	query = db_session.execute("SELECT id, target_phone_number, send_switch, nickname FROM sms_targets WHERE user_key = :user_key AND del = 0",\
		{'user_key' : session['userkey']})
	result = []
	if query:
		for row in query:
			result.append({'id' : row[0], 'nickname' : row[3].decode('utf8'), 'target_phone_number' : row[1].decode('utf8'), 'send_switch' : row[2]})
	return jsonify(result)

@config.route('/sms', methods=["POST"])
@login_required
def send_sms():
	posted = request.get_json()
	query = db_session.execute("SELECT target_phone_number, send_switch FROM sms_targets WHERE user_key = :user_key", {'user_key' : session['userkey']})
	if query:
		for row in query:
			if row[1]:
				sms = Sms(title='', to_number=row[0], from_number='', desc=posted['desc'].encode('utf8'))
				sms.send()
		return ('', 200)
	else:
		return ('', 204)

@config.route('/target/sms', methods=['POST', 'PUT', 'DELETE'])
@login_required
def add_sms_target():
	if request.method == 'POST':
		posted = request.get_json()
		committed = db_session.execute("INSERT INTO sms_targets (user_key, target_phone_number, send_switch, nickname) VALUES (%s, '%s', %s, '%s')" % (session['userkey'], posted['target_phone_number'], 1, posted['nickname']))
		db_session.commit()
		query = db_session.execute("SELECT id, target_phone_number, send_switch, nickname FROM sms_targets WHERE id = :id AND del = 0", {'id' : committed.lastrowid})
		result = {}
		for row in query:
			result = {'id' : row[0], 'nickname' : row[3].decode('utf8'), 'target_phone_number' : row[1].decode('utf8'), 'send_switch' : row[2]}
			break
		return jsonify(result)
	elif request.method == 'PUT':
		posted = request.get_json()
		print(posted)
		committed = db_session.execute("UPDATE sms_targets SET send_switch = :send_switch WHERE id = :phone_id",\
		{'send_switch' : posted['send_switch'], 'phone_id' : posted['id']})
		db_session.commit()
		query = db_session.execute("SELECT id, target_phone_number, send_switch, nickname FROM sms_targets WHERE id = :id AND del = 0", {'id' : posted['id']})
		result = {}
		for row in query:
			result = {'id' : row[0], 'nickname' : row[3].decode('utf8'), 'target_phone_number' : row[1].decode('utf8'), 'send_switch' : row[2]}
			break
		return jsonify(result)
	elif request.method == 'DELETE':
		db_session.execute("UPDATE sms_targets SET del = 1 WHERE id = :phone_id", \
			{'phone_id' : request.args.get('id')})
		db_session.commit()
		return ('', 200)

@config.route('/target/time', methods=['GET', 'POST', 'DELETE'])
@login_required
def target_time():
	if request.method == 'GET':
		query = db_session.execute("SELECT time FROM time_schedule WHERE type = 'SMS' AND user_key = :user_key ORDER BY time ASC", {'user_key' : session['userkey']})
		result = []
		for row in query:
			result.append(row[0])
		return jsonify(result)
	elif request.method == 'POST':
		posted = request.get_json()
		query = db_session.execute("INSERT INTO time_schedule (user_key, time, type) VALUES (%s,%s,'%s')" % (session['userkey'], posted['target_time'], 'SMS'))
		db_session.commit()
		return ('', 200)
	elif request.method == 'DELETE':
		query = db_session.execute("DELETE FROM time_schedule WHERE user_key = :user_key AND time = :target_time", {'user_key' : session['userkey'], 'target_time' : request.args.get('target_time')})
		db_session.commit()
		return ('', 204)
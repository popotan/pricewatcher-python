from flask import Blueprint, render_template, jsonify, request, session, redirect
from priceWatcher.database import init_db
from priceWatcher.database import db_session
# from priceWatcher.model.user import UserORM
from flask_bcrypt import generate_password_hash
import datetime, json
import urllib.parse
import urllib.request

from priceWatcher.custom_module.decorator import login_required

user = Blueprint('user', __name__)

@user.route('/')
@login_required
def index():
	return ('', 200)

@user.route('/login', methods=["POST"])
def login():
	posted = request.get_json()

	query = db_session.execute('SELECT user_key, id, sheetname FROM users WHERE id = :id AND password = :password',\
	{'id' : posted['id'].encode('utf8'), 'password' : posted['password'].encode('utf8')})

	if query:
		for row in query:
			mk_session(row[0], row[1], row[2])
		return ('', 200)
	else:
		return ('', 401)

@user.route('/logout', methods=['GET', 'POST'])
def logout():
	session.clear()
	return ('', 200)

def mk_session(user_key, email, sheetid):
	session['userkey'] = user_key
	session['id'] = email
	session['sheetid'] = sheetid
	session['valid'] = True
	print(session)